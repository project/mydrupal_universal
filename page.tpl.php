<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head profile="http://gmpg.org/xfn/11">
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<div id="page"><!-- begin page -->
  <div id="header"><!-- begin header -->
    <?php if (isset($secondary_links)) { ?><div id="secondary"><?php print theme('primary', $secondary_links) ?></div><?php } ?>
   <?php if ($logo) { ?><div id="logo"><a href="<?php print $base_path ?>" title="<?php print $site_name ?>"><img src="<?php print $logo ?>" alt="<?php print $site_name ?>" /></a></div><?php } ?>
    <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a></h1><?php } ?>
    <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>

    <?php print $header ?>
  </div><!-- end header -->
  <?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('primary', $primary_links) ?></div><?php } ?>
        <?php print $breadcrumb ?>
  <?php print $search_box ?>
  <div id="content"><!-- begin content -->
    <?php if ($sidebar_right) { ?><div id="main"><!-- begin main --><?php } ?>
      <?php if ($mission) { ?>
        <div id="mission">
          <h2 class="title"><?php print t('Mission') ?></h2>
          <div class="content"><?php print $mission ?></div>
        </div>
      <?php } ?> 
      <?php if ($title) { ?><h1 class="title"><?php print $title ?></h1><?php } ?>
      <div class="tabs"><?php print $tabs ?></div>
      <?php print $help ?>
      <?php print $messages ?>
      <?php print $content; ?>
    <?php if ($sidebar_right) { ?></div><!-- end main --><?php } ?>
    <?php if ($sidebar_right) { ?>
      <div id="sidebar-right"><!-- begin sidebar-right -->
        <?php print $sidebar_right ?> 
      </div><!-- end sidebar-right -->
    <?php } ?>
  </div><!-- end content -->
  <div id="footer"><!-- start footer -->
    <?php print $footer_message ?>
  </div><!-- end footer -->
  <div id="designer">Sponsored by: <a href="http://whiz.in"> Domain Name </a> &nbsp;&nbsp|&nbsp;&nbsp;<a href="http://itdiscover.com">IT Links </a> &nbsp;&nbsp|&nbsp;&nbsp; <a href="http://techjobs.co.in">Tech Jobs</a> &nbsp;&nbsp; Theme by: <a href="http://mydrupal.com">My Drupal</a>
 
</div><!-- end page -->
<?php print $closure ?>
  </div>
</body>
</html>
