<?php
function phptemplate_search_theme_form($form) {
  return _phptemplate_callback('search-theme-form', array('form' => $form));
}

function MyDrupal_Universal_primary($items = array()) {
  $menu = menu_get_item(variable_get('menu_primary_menu', 0));
  $output = '<div class="title">'  . '</div>';
  $output .= theme('links', $items);
  return $output;
}

function MyDrupal_Universal_regions() {
  return array(
    'right' => t('right sidebar'),
    'content' => t('content'),
    'header' => t('header'),
    'footer' => t('footer'),
  );
}
