<div class="node<?php print $sticky ? ' sticky' : '' ?><?php print $status ? '' : ' node-unpublished' ?>">
  <div class="itemhead"> 
    <?php if ($page == 0): ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title ?></a></h2><?php endif; ?>
    <div class="metadata">
      <?php if ($picture): print $picture; endif; ?>
      <?php if (node_access('update', $node)): print l('', 'node/' . $node->nid . '/edit', array('class' => 'editlink', 'title' => t('Edit'))); endif;?>
      <?php if ($submitted): ?><div class="chronodata"><?php print $submitted ?></div><?php endif; ?>
      <?php if ($terms): ?><div class="tagdata"><?php print $terms ?></div><?php endif; ?>
    </div>
  </div> 
  <div class="content">
    <?php print $content?>
    <div class="metadata">
      <?php if ($links): ?><div class="commentslink"><?php print $links ?></div><?php endif; ?>
    </div>
  </div>
</div>
