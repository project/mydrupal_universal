<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>"> 
  <div class="itemhead">
    <h3 class="title"><?php print $title; ?></h3><?php if ($new != '') { ?><span class="new"><?php print $new; ?></span><?php } ?>
    <div class="metadata">
      <?php if ($picture): print $picture; endif; ?>
      <?php if ($submitted): ?><div class="chronodata"><?php print $submitted ?></div><?php endif; ?>
    </div>
  </div>
  <div class="content">
    <?php print $content; ?>
    <div class="metadata">
      <?php if ($links): ?><div class="commentslink"><?php print $links ?></div><?php endif; ?>
    </div>
  </div>
</div>
