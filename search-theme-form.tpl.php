<?php
  global $user;

  if (!$user->uid) {
    $message .= '<div>' . t('Please <a href="@login">Login</a> or <a href="@register">Register</a>', array('@login' => url('user/login'), '@register' => url('user/register'))) . '</div>';
    $message .= '<div>' . t('<a href="@password">Request New Password</a>', array('@password' => url('user/password'))) . '</div>';
  } 
  else {
    $message .= '<div>' . t('Welcome @user', array('@user' => $user->name)) . '</div>';
    $message .= '<div>' . t('<a href="@view">View</a> | <a href="@edit">Edit</a> | <a href="@logout">Logout</a>', array('@view' => url('user/' . $user->uid), '@edit' => url('user/' . $user->uid . '/edit'), '@logout' => url('logout'))) . '</div>';
  }
?>

<div class="welcome"><?php print $message ?></div>
<div class="search-form">
  <div class="title"><?php print t('Search the Site:') ?></div>
  <?php print drupal_render($form['search_theme_form_keys']) ?>
  <?php print drupal_render($form['submit']) ?>
  <?php print drupal_render($form) ?>
  <div class="advanced"><?php print l(t('Advanced'), 'search') ?></div>
</div>
